//Оформить решения ДЗ лекций 4-5 в виде функций.

// Задание 4.1: Создать переменную 1 с пустой строкой. Установить для нее значение. Создать еще одну переменную 2 со строкой и значением. Сделать проверку, если переменная 1 не такая же как переменная 2, то присвоить переменной 1 значение переменной 2. Распечатать промежуточные результаты и добавить комментарии для кода.


func taskLesson4_1 (strPar1: String, strPar2: String) -> String {
    
    var str1 = strPar1
    let str2 = strPar2
    
    if str1 != str2 {
        str1 = str2
        print("Переменная str1 = \(str1)")
    } else {
        print("Переменные равны: str1 = str2")
    }
    
    return (str1)
}

taskLesson4_1(strPar1: "Привет!", strPar2: "Привет!")


// Задание 4.2: Есть 2 строковые константы. Проверить какая из них длиннее и вывести в консоль первый и последний символы строки которая длиннее.

func taskLesson4_2 () {
    
    let strConstOne = "Скоро Новый год 2021"
    let strConstTwo = "Рождество скоро!"
    
    
    let numberStrConstOne = strConstOne.count
    print("\nКол-во символов в первой строке: \(numberStrConstOne)")
    let numberStrConstTwo = strConstTwo.count
    print("Кол-во символов во второй строке: \(numberStrConstTwo)")
    
    
    // выполнение вывода первого и последнего символа большей строки
    if numberStrConstOne > numberStrConstTwo {
        let firstIndex = strConstOne.startIndex
        let firstSimvol = strConstOne[firstIndex]
        print("\nПервый символ в строке: \"\(firstSimvol)\"")
        let lastIndex = strConstOne.index(strConstOne.endIndex, offsetBy: -1)
        let lastSimvol = strConstOne[lastIndex]
        print("Последний символ в строке: \"\(lastSimvol)\"\n")
    } else {
        let firstIndex = strConstTwo.startIndex
        let firstSimvol = strConstTwo[firstIndex]
        print("\nПервый символ в строке: \"\(firstSimvol)\"")
        let lastIndex = strConstTwo.index(strConstTwo.endIndex, offsetBy: -1)
        let lastSimvol = strConstTwo[lastIndex]
        print("Последний символ в строке: \"\(lastSimvol)\"\n")
    }
}

taskLesson4_2 ()


// Задание 4.3: Есть константа со строкой длиной 6 символов. Нужно распечатать эту строку в обратном порядке, при этом распечатывая строку посимвольно.

func taskLesson4_3 (strSixSimvols: String = "привет") {
    
    // шестой символ
    let indexSimvol_6 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 5)
    let simvol_6 = strSixSimvols[indexSimvol_6]
    print(simvol_6)
    
    // пятый сивол
    let indexSimvol_5 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 4)
    let simvol_5 = strSixSimvols[indexSimvol_5]
    print(simvol_5)
    
    // четвертый символ
    let indexSimvol_4 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 3)
    let simvol_4 = strSixSimvols[indexSimvol_4]
    print(simvol_4)
    
    // третий символ
    let indexSimvol_3 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 2)
    let simvol_3 = strSixSimvols[indexSimvol_3]
    print(simvol_3)
    
    // второй символ
    let indexSimvol_2 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 1)
    let simvol_2 = strSixSimvols[indexSimvol_2]
    print(simvol_2)
    
    // первый символ
    let indexSimvol_1 = strSixSimvols.startIndex
    let simvol_1 = strSixSimvols[indexSimvol_1]
    print(simvol_1)
    
}

taskLesson4_3()


// Задание 5.1: Дано число произвольной длины. Найти сумму и произведение его цифр.

func taskLesson5_1() {
    var x = 12345
    var y: Int
    var myArray: Array<Int> = []
    
    repeat {
        y = x % 10
        x = (x - y) / 10
        myArray.append(y)
    } while x != 0
    
    var sum = 0
    var mult = 1
    
    for i in myArray {
        sum += i
        mult *= i
    }
    
    print("\nСумма всех цифр: \(sum)\nПроизведение всех цифр равно: \(mult)")
}

taskLesson5_1()


//  Задание 5.2: Дано число, дана степень. Возвести число в степень используя for-in.

func taskLesson5_2(){
    let chislo = 2
    let stepen = 3
    var result = 1
    
    for _ in 1 ... stepen {
        result *= chislo
    }
    
    print("\n\(chislo) в степени \(stepen) равно \(result)")
}

taskLesson5_2()


// Задание 5.3: Дано: массив типа Float с произвольным количеством элементов. Вычислить среднее арифметическое элементов массива.

func taskLesson5_3() {
    let myArrayFloat: Array<Float> = [4.7, 2.8, 5.9, 4.0, 22.7]
    
    let colichestvo = myArrayFloat.count
    var sumElements: Float = 0
    
    for j in myArrayFloat {
        sumElements += j
    }
    
    let sredArifm: Float = sumElements / Float(colichestvo)
    
    print("\nСреднее арифметическое элементов массива: \(sredArifm)")
    
}

taskLesson5_3()


