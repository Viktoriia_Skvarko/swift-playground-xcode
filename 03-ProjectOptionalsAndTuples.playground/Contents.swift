// опциональные значения, кортежи, операторы сравнения, логические операторы, конструкцию If – else и тернарный оператор

var ageStudentDjack = "23"
var ageStudentNatali = 26
var ageStudentDilan = 28

var studentDjack = (firstName: "Djack", lastName: "Smith", aveageScore: 3.7)
var studentNatali = (firstName: "Natali", lastName: "Black", aveageScore: 4.2)
var studentDilan = (firstName: "Dilan", lastName: "Kets", aveageScore: 5.0)


var averageAgeStudents = (Int(ageStudentDjack)! + ageStudentNatali + ageStudentDilan) / 3
print("Средний возраст студентов: \(averageAgeStudents) лет")


if (Int(ageStudentDjack)! < ageStudentNatali) && (Int(ageStudentDjack)! < ageStudentDilan) {
    print("\(studentDjack.firstName) \(studentDjack.lastName) самый младший студент в групппе")
} else if (ageStudentNatali < Int(ageStudentDjack)!) && (ageStudentNatali < ageStudentDilan) {
    print("\(studentNatali.firstName) \(studentNatali.lastName) самая младшая студентка в групппе")
} else {
    print("\(studentDilan.firstName) \(studentDilan.lastName) самый младший студент в групппе")
}


let groupAveageScore = (studentDjack.aveageScore + studentNatali.aveageScore + studentDilan.aveageScore) / 3
print("Средний бал группы: \(groupAveageScore)")


// использование тренарного оператора:
print((studentDjack.aveageScore >= groupAveageScore) ? "Джек преуспевающий студент" : "Джеку нужно подтянуть средний бал")


//конструкция if else аналог того, что сделано с использованием тренарного оператора:
if (studentDjack.aveageScore >= groupAveageScore) {
    print("Джек преуспевающий студент")
} else {
    print("Джеку нужно подтянуть средний бал")
}
