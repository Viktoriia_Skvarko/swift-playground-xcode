// Создать переменную 1 с пустой строкой. Установить для нее значение. Создать еще одну переменную 2 со строкой и значением. Сделать проверку, если переменная 1 не такая же как переменная 2, то присвоить переменной 1 значение переменной 2. Распечатать промежуточные результаты и добавить комментарии для кода.


// объявление переменных:
var str1: String = ""
var str2 = "Привет!"

if str1 != str2 {    // проверка равенства переменных
    str1 = str2     // присвоение str1 значения str2
    print("Переменная str1 = \(str1)")    // печать нового значения str1
} else {
    print("Переменные равны: str1 = str2")
}



// Есть 2 строковые константы. Проверить какая из них длиннее и вывести в консоль первый и последний символы строки которая длиннее.


// объявление переменных
let strConstOne = "Скоро Новый год 2021"
let strConstTwo = "Рождество скоро!"


// вычисление кол-ва символов в строках
var numberStrConstOne = strConstOne.count
print("\nКол-во символов в первой строке: \(numberStrConstOne)")
var numberStrConstTwo = strConstTwo.count
print("Кол-во символов во второй строке: \(numberStrConstTwo)")


// выполнение вывода первого и последнего символа большей строки
if numberStrConstOne > numberStrConstTwo {
    let firstIndex = strConstOne.startIndex
    let firstSimvol = strConstOne[firstIndex]
    print("\nПервый символ в строке: \"\(firstSimvol)\"")
    let lastIndex = strConstOne.index(strConstOne.endIndex, offsetBy: -1)
    let lastSimvol = strConstOne[lastIndex]
    print("Последний символ в строке: \"\(lastSimvol)\"\n")
} else {
    let firstIndex = strConstTwo.startIndex
    let firstSimvol = strConstTwo[firstIndex]
    print("\nПервый символ в строке: \"\(firstSimvol)\"")
    let lastIndex = strConstTwo.index(strConstTwo.endIndex, offsetBy: -1)
    let lastSimvol = strConstTwo[lastIndex]
    print("Последний символ в строке: \"\(lastSimvol)\"\n")
}



// Есть константа со строкой длиной 6 символов. Нужно распечатать эту строку в обратном порядке, при этом распечатывая строку посимвольно.

let strSixSimvols = "привет"

// шестой символ
var indexSimvol_6 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 5)
var simvol_6 = strSixSimvols[indexSimvol_6]
print(simvol_6)

// пятый сивол
var indexSimvol_5 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 4)
var simvol_5 = strSixSimvols[indexSimvol_5]
print(simvol_5)

// четвертый символ
var indexSimvol_4 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 3)
var simvol_4 = strSixSimvols[indexSimvol_4]
print(simvol_4)

// третий символ
var indexSimvol_3 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 2)
var simvol_3 = strSixSimvols[indexSimvol_3]
print(simvol_3)

// второй символ
var indexSimvol_2 = strSixSimvols.index(strSixSimvols.startIndex, offsetBy: 1)
var simvol_2 = strSixSimvols[indexSimvol_2]
print(simvol_2)

// первый символ
var indexSimvol_1 = strSixSimvols.startIndex
var simvol_1 = strSixSimvols[indexSimvol_1]
print(simvol_1)
