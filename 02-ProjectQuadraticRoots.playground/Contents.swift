// Написать код для решения квадратного уравнения, где a, b, c – целые переменные, нужно найти x1 и x2

import UIKit
import Foundation

var a: Double = 1.0
var b: Double = -3.0
var c: Double = -4.0

var discriminant = (b * b) - (4 * a * c)
print("Дискрименант равен: \(discriminant)")

var x1: Double
var x2: Double

if discriminant > 0 {
    x1 = (-b + sqrt(discriminant)) / (2 * a)
    x2 = (-b - sqrt(discriminant)) / (2 * a)
    print("x1 = \(x1)\nx2 = \(x2)")
} else if discriminant == 0 {
    x1 = (-b) / (2 * a)
    x2 = x1
    print("x1 = x2 =\(x1)")
} else {
    print("Корней нет")
}
