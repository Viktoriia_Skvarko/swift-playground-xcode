// Дано число произвольной длины. Найти сумму и произведение его цифр.

var x = 12345
var y: Int
var myArray: Array<Int> = []

repeat {
    y = x % 10
    x = (x - y) / 10
    myArray.append(y)
} while x != 0

var sum = 0
var mult = 1

for i in myArray {
    sum += i
    mult *= i
}

print("Сумма всех цифр: \(sum)\nПроизведение всех цифр равно: \(mult)")



// Дано число, дана степень. Возвести число в степень используя for-in.

var chislo = 2
var stepen = 3
var result = 1

for _ in 1 ... stepen {
    result *= chislo
}
print("\n\(chislo) в степени \(stepen) равно \(result)")



// Дано: массив типа Float с произвольным количеством элементов. Вычислить среднее арифметическое элементов массива.

var myArrayFloat: Array<Float> = [4.7, 2.8, 5.9, 4.0, 22.7]

var colichestvo = myArrayFloat.count
var sumElements: Float = 0

for j in myArrayFloat {
    sumElements += j
}

var sredArifm: Float = sumElements / Float(colichestvo)
print("\nСреднее арифметическое элементов массива: \(sredArifm)")
