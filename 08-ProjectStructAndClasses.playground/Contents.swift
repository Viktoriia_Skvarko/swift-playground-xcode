// Написать программу в Playground, которая содержит объявление класса согласно условию и примеры использования класса. Объекты — дома в квартале (квартал — “хранилище” домов). Поля (атрибуты): номер дома, количество этажей, количество квартир на этаже, количество комнат в каждой квартире этого дома. Методы: вывод всей информации, вывод количества квартир в подъезде, вывод количества комнат в квартирах одного подъезда, проверка наличия квартиры в доме по ее номеру, поиск подъезда и этажа по номеру квартиры.

import Foundation

class Dom {
    var nomerDoma: Int
    var kolPodyezdov: Int
    var kolKvarNaEtaze: Int
    var kolEtazey: Int
    var kolKomnKvartira: Int
    
    init(nomerDoma: Int, kolPodyezdov: Int, kolKvarNaEtaze: Int, kolEtazey: Int, kolKomnKvartira: Int) {
        self.nomerDoma = nomerDoma
        self.kolPodyezdov = kolPodyezdov
        self.kolKvarNaEtaze = kolKvarNaEtaze
        self.kolEtazey = kolEtazey
        self.kolKomnKvartira = kolKomnKvartira
        
    }
    
    
    
    // метод - вся информ по дому:
    func allInformDom() {
        let kolKvartirPodyezd = kolEtazey * kolKvarNaEtaze
        let kolKvartDom = kolKvartirPodyezd * kolPodyezdov
        print("Вся информация по дому #\(nomerDoma):")
        print("Квартир в доме: \(kolKvartDom) \nЭтажность дома: \(kolEtazey) \nКоличество подъездов в доме: \(kolPodyezdov) \nКоличество квартир в подъезде: \(kolKvartirPodyezd)")
    }
    
    
    // метод - количество квартир в подъезде:
    func kolKvarPod() -> Int {
        let kolKvartirPodyezd = kolEtazey * kolKvarNaEtaze
        return kolKvartirPodyezd
    }
    
    
    // метод - количество комнат в квартирах одного подъезда:
    func kolKomnatPodyezd() -> Int {
        let kolKvartirPodyezd = kolEtazey * kolKvarNaEtaze
        let komnPod = kolKvartirPodyezd * kolKomnKvartira
        return komnPod
    }
    
    
    // метод - проверка наличия квартиры по ее номеру (входящий параметр - номер квартиры, которую ищем):
    func proverkaKvartiryPoNomery(kvartiraLooking: Int) -> Bool {
        let kolKvartirPodyezd = kolEtazey * kolKvarNaEtaze
        let kolKvartDom = kolKvartirPodyezd * kolPodyezdov
        if kvartiraLooking <= kolKvartDom {
            return true
        } else {
            return false
        }
    }
    
    
    // метод - вычисление подъезда и этажа искомой квартиры
    func dannieProKvartiry(kvartiraLooking: Int) {
        let kolKvartirPodyezd = kolEtazey * kolKvarNaEtaze
        let kolKvartDom = kolKvartirPodyezd * kolPodyezdov
        if kvartiraLooking <= kolKvartDom {
            let nomerPodOstat = kvartiraLooking % kolKvartirPodyezd
            var nomerPod = kvartiraLooking / kolKvartirPodyezd
            if nomerPodOstat > 0 {
                nomerPod += 1
            } else {
                nomerPod = kvartiraLooking / kolKvartirPodyezd
            }
            let etazSeach = kvartiraLooking - (kolKvartirPodyezd * (nomerPod - 1))
            let etazOst = etazSeach % kolKvarNaEtaze
            var etaz = etazSeach / kolKvarNaEtaze
            if etazOst > 0 {
                etaz += 1
            } else {
                etaz = etazSeach / kolKvarNaEtaze
            }
            print("Квартира \(kvartiraLooking) находится в \(nomerPod) подъезде на \(etaz) этаже")
        } else {
            print("В доме #\(nomerDoma) нет квартиры \(kvartiraLooking)")
        }
    }
    
    
}



// Проверка работы методов:

let informDom = Dom(nomerDoma: 1, kolPodyezdov: 4, kolKvarNaEtaze: 4, kolEtazey: 9, kolKomnKvartira: 2)


//вызов метода - вся информация по дому:
//informDom.allInformDom()

//вызов метода - количество квартир в подъезде:
//print("Количество квартир в подъезде дома #\(informDom.nomerDoma) : \(informDom.kolKvarPod())")


//вызов метода - количество комнат в квартирах одного подъезда:
//print("Количество комнат в квартирах одного подъезда дома #\(informDom.nomerDoma) : \(informDom.kolKomnatPodyezd())")


//вызов метода - проверка аличия квартиры по ее номеру:
//var nomerKv = informDom.proverkaKvartiryPoNomery(kvartiraLooking: 15)
//if nomerKv == true {
//    print("В доме есть такая квартира")
//} else {
//    print("В доме нет такой квартиры")
//}


// вызов метода - вычисление подъезда и этажа искомой квартиры:
//informDom.dannieProKvartiry(kvartiraLooking: 135)
