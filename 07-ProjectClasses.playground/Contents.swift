// Создать класс Tile (плитка), который будет содержать свойства: brand, sizeHeight, sizeWidth, price и метод класса description(). Объявить пару объектов класса и внести данные в свойства. Затем вернуть их в виде удобочитаемой строки, вызвав метод description(), а затем вывести в консоль.

class Tile {
    var brand: String = "Zara"
    var sizeHeight: Int = 110
    var sizeWidth: Int = 30
    var price: Double = 23.50
    
    func description() -> String {
        return "Изделие:"
    }
    
    init(brandParam: String, sizeHeightParam: Int, sizeWidthParam: Int, priceParam: Double) {
        brand = brandParam
        sizeHeight = sizeHeightParam
        sizeWidth = sizeWidthParam
        price = priceParam
    }
}

let myTile = Tile.init(brandParam: "My Brnd", sizeHeightParam: 90, sizeWidthParam: 25, priceParam: 57.50)
var method = myTile.description()
print(method)
print("Бренд: \(myTile.brand), Длина: \(myTile.sizeHeight), Ширина: \(myTile.sizeWidth), Цена: \(myTile.price)")




// Создать класс с именем Student, содержащий поля: фамилия и инициалы, номер группы, успеваемость (массив из пяти элементов). Создать массив из десяти элементов такого класса. Вывести фамилии и номера групп студентов, имеющих оценки, равные только 4 или 5 (функцией и циклом).

class Student {
    var firstName: Array<String>
    var inicialLastName: Array<String>
    var inicialOtchestvo: Array<String>
    var group: Array<Int>
    var sredBal: Array<Int>
    
    init(firstName: Array<String>, inicialLastName: Array<String>, inicialOtchestvo: Array<String>, group: Array<Int>, sredBal: Array<Int>)
    {
        self.firstName = firstName
        self.inicialLastName = inicialLastName
        self.inicialOtchestvo = inicialOtchestvo
        self.group = group
        self.sredBal = sredBal
    }
}

var studentIsElemClassa = Student(firstName: ["Smith", "Djon", "Harris", "Johnson", "Williams", "Brown", "Scott", "Walker", "Edwards", "Turner"],
                                  inicialLastName: ["D", "S", "R", "G", "R", "B", "N", "Y", "O", "E"],
                                  inicialOtchestvo: ["V", "B", "M", "T", "B", "F", "Y", "Y", "W", "B"],
                                  group: [1, 2, 4, 3, 2, 3, 5, 2, 6, 7],
                                  sredBal: [4, 3, 5, 2, 3, 3, 4, 3, 5, 4])

print("\nСтуденты с средним балом 4 и 5:") // эта строка, чтобы визуально разделить два задания в выводе

for i in 0 ..< studentIsElemClassa.sredBal.count {
    if (studentIsElemClassa.sredBal[i] == 4 || studentIsElemClassa.sredBal[i] == 5) {
        print("\(studentIsElemClassa.firstName[i]) \(studentIsElemClassa.inicialLastName[i])\(studentIsElemClassa.inicialOtchestvo[i]), Group: \(studentIsElemClassa.group[i])")
    }
}

